# Define Your Atoms

Atoms are chunks of data which attesters can produce. All attesters should define what type of data they will produce. These atoms definitions are then used by other attesters to be used in their `requiredForLogin` section.

## Atom Definition

The above CID gives following content. This is atom definition. Each atom definition contains `name`, `type`, `attester`. <!-- Should there be an example CID above? -->

- `name` is the name of atom. This is displayed in UI whenever we need to show the atom.
- `type` is the type of data it can hold. Possible types of atoms are `string`, `number`, `date`, `boolean`, `json`.
- `attester` is the DID of the attester.

```json
{"name":"firstName","type":"string","attester":"did:mdip:omni-xuh4-5y9r-qp6f-905"}
```

![image](https://docs.yourself.id/files/set-available.png)

In the UI, you will be able to set the available molecules with ease. You will have to enter name & select one of type from dropdown. In DID doc these atom definitions are stored in form of CIDs.

Here is an example of `availableMolecules`.

<!-- Should this array be renames to availableAtoms? -->

```json
    "availableMolecules": [
        "Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa",
        "QmWsSigdhVNBn3uyNZi12L599HxX8G6z4orQKsaR9EN8Mb",
        "QmX6C4hvDBNyKR3GiDQxyRxykwUsY2Dcpwbcbs1wfMpkvA",
        "QmSH3p1FbyE8VfKovFJzj7Tu8rwTAUPHKzWMryxNebiA6c",
        "QmY3mCa53xeYsUTwWcR2eppGHp3gJSn2mQVD71cmceaFA6",
        "Qme7wm9HqRENy6famYP5Qk9g13xvXEm4SNn3QWGGAeYM9V",
        "QmfVrnoyVgiMjQFfwbyzJTDDsBiRLjCqVsPzYg6zByZusv",
        "QmeCSLSZHkL3N1dBNtNXYNtp9ZeP3L5Y8F4fcvFceyFRR6",
        "QmU5rX8Eb6yzy24j9ZgJwHKgrcSQNA28V6BFFCrgZwYhg9",
        "QmekHwwXoBqAgmBnWER5TZUfgXGLnEWh4AxTza5h1reNX5",
        "QmXSEtBvTyf2RJNTmNWiAF9WEvjX7pnTK19GmFXGxq2t1o"
    ]
```

Check cid [Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa](https://gateway.pinata.cloud/ipfs/Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa#x-ipfs-companion-no-redirect) this relates for atom `firstName` of type string. <!-- This link doesn't work, not sure what this is trying to say. -->
