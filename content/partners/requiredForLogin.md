# Required for login

This is more of a design choice as the required for login atoms should be present before user can login to your system. So attesters should decide which other attesters they should depend on for information.

![image](https://docs.yourself.id/files/select-category.png)

Required for login is setup at the time of onboarding. You can select entity from dropdown. After a while all the related atoms are shown on the `DNA Molecules` section.

Here is the sample `requiredForLogin` field value. This value is added to IPFS file for you by us.

```
"requiredForLogin": [
        "Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa",
        "QmWsSigdhVNBn3uyNZi12L599HxX8G6z4orQKsaR9EN8Mb",
        "QmX6C4hvDBNyKR3GiDQxyRxykwUsY2Dcpwbcbs1wfMpkvA",
        "QmSH3p1FbyE8VfKovFJzj7Tu8rwTAUPHKzWMryxNebiA6c",
        "QmY3mCa53xeYsUTwWcR2eppGHp3gJSn2mQVD71cmceaFA6",
        "QmfVrnoyVgiMjQFfwbyzJTDDsBiRLjCqVsPzYg6zByZusv"
    ]
```

In above example, `requiredForLogin` has an array of CIDs. You can fetch content inside these CIDs via any IPNS public gateways. Check cid [Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa](https://gateway.pinata.cloud/ipfs/Qme7eV6hmJiDuedfLrYDPLm1M6gyjCTB8mraQfRBTjbVEa#x-ipfs-companion-no-redirect).


![image](https://docs.yourself.id/files/ipfs.png)

Now all you have to do is fetch the IPNS CID to use it in your backend environment. In above image, you can fetch IPFS from console log after few mins of refreshing onboarding window.


Notes:

- One attestor can depend on multiple attesters at a time.
- Default Login with selfid flow doesn't require any information from other attesters.
- This is not mendatory for all attesters.

