# Testing

Before going through this document, please make sure you have already completed your onboarding in marketplace portal. You can confirm the process is complete, if you are able to visit `Account Settings` from top right dropdown menu.


Firstly, if you are planning to send attestations to users, then it is necessary to define all atoms from `Set Available Molecules` menu in onboarding protal. This is necessary step because you would not be able to send data if you have not defined data types in above mentioned page.


Secondly, if you are planning to use attestations provided by other attesters as `requiredForLogin` (meaning necessary for login) then please define them via `Required For Login` menu item in onboarding protal. If you dont need that you can move on to testing right away.


# Testing normal login flow
1. Go through [backend](./self-backend.md) integration docs. If you are in a hurry to see integration in action just clone the [repo](https://gitlab.com/selfid-public/self-sample-attestor-express/-/tree/dish-self-login/) branch `dish-self-login`.
1. Access all necessary environment variables needed mentioned in [envinment](./environment.md) documentation docs.
1. Create `.env` file and store all environment variables from previous step
1. Add debug environment variable `DEBUG=self-sdk:*`
1. Test the websocket integration
1. Test self login


## Test websocket integration

You can test if websocket integration is working by following :

- run the backend server by using command `npm start`
- copy paste the following html code in empty html file
- update the `url` in html code below (if it isnt localhost, ten update it to a proper domain, with `WSS`)
- click on `Sign in with your SELF` button & see if you are able to see QR. If you are able to see QR then websocket setup is working fine.

```html
<html>
  <head>
    <meta charset="UTF-8" />
    <script
      type="text/javascript"
      src="https://cdn.yourself.id/btn.js"
    ></script>
    <script>
      const url = "ws://localhost:3000";
      console.log("url", url);
      function loginSuccess(data) {
        console.log("data on success ::", data);
        document.getElementById("claimData").textContent = JSON.stringify(
          data.claimData,
          undefined,
          2
        );
        document.getElementById("qrcode").textContent = "";
      }
      createBtn(url, loginSuccess);
    </script>
  </head>

  <body>
    <h3>WSS test</h3>
    <div id="qrcode"></div>
    <pre id="claimData"></pre>
  </body>
</html>
```

## Test Self login

1. Follow steps mentioned in `Testing normal login flow`
1. Install `SELF` app from test flight
1. Make sure you have defined available molecules (if you are planning to send attestations)
1. make sure you have required for login molecules in your gnome inside the SELF app
1. Now test WS integration
1. Drag down white circle in app to scan the QR generated in WS integration testing
1. If you have required for login molecules in your app's gnome you should be able to click on `ACCEPT ALL` button on screen
1. After clicking on `ACCEPT ALL` & waiting for few seconds you should see `Successfully logged in to {attester name}` message on your `SELF` app
1. Incase of any error you should be able to track it by using debug environment variable (mentioned above & in [envrionment](./environment.md) docs)

Please make sure you are sending `{success: true}` as successful reponse from backend.



## Test Self connect

You will need to understand self connect concept first. `Self connect` is necessary in cases where your business logic only lets user connect to self after letting them login via normal authentication process. Normal login flow tries to replace the original auth process, whereas `Self connect` can be used as add on to your current business flow.

Things to note.
- UI will show `Sign in with your SELF` button only afterr logging in via their own authentication system
- `clientId` is crucial in `Self connect`. So you should treat it as secure as auth token. You can treat it as unique ID assigned to client session. It should be unique for each session.
- You will have to store `clientId` in frontend store and add it to `createBtn` function call as mentioned in [frontend](./self-frontend.md) documentation.


Now follow this [repo](https://gitlab.com/selfid-public/self-sample-attestor-express/-/blob/updated-self-login/) to create your code. After this you should be able to login via the frontend. After logging you can see `Sign in with your SELF` button. Rest of the testing in similar to `Test Self login` section.
