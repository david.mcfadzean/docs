# Backend Environment Variables

## DOMAIN

`DOMAIN` is the domain of the server you are planning to deploy. This is used to hit the API endpoints for login & payments etc. When a user clicks on "login with SELF", the UI will make a websocket connection with this domain.

The SELF mobile apps require a secure HTTPS connection. If your backend is not configured to serve directly over HTTPS, use a reverse proxy to create a secure endpoint.

## SELF_MARKETPLACE_TOKEN

![image](../files/api-token.png)

`SELF_MARKETPLACE_TOKEN` is a marketplace token. You can generate your API token during the [Onboarding Process](./onboarding.mdx), or at any time from the **API Access Token** option in the main menu.

## ATTESTOR_NAME

`ATTESTOR_NAME` is the name shown to users on the mobile UI. This should be the same supplied in the Add platform details step.

## ATTESTOR_DID

![image](../files/setting-ipns.png)

`ATTESTOR_DID` can be accessed via the Account setting option on the top right menu on onboarding website. <!-- Need more about what this is/does. -->

## Keys and Hexes

![image](../files/app_keys.png)

`PUBLIC_KEY`, as the name suggests, is the public key of your account. It should be saved at the App keys step (and later in settings page).

`PRIVATE_KEY` is the private key of your account. You can get your private key at the App keys step (and later in settings page). *You must not share this value with anyone else*, otherwise anyone could have access to the wallet and funds from your account.

`PUBLIC_KEY_HEX`: This field is accessible at App keys page as shown in above screenshot (and later in settings page). <!-- What's it for? -->

`PRIVATE_KEY_HEX`: This field is accessible at App keys page as shown in above screenshot (and later in settings page). You must not share this value with anyone else otherwise anyone could have access to wallet funds from your account.

![image](../files/setting-keys.png)

If by any change you forgot to store above mentioned ENV values, you can check `PUBLIC_KEY`, `PRIVATE_KEY`, `PUBLIC_KEY_HEX` & `PRIVATE_KEY_HEX` from settings page.

## PAYMENT_ENABLED

`PAYMENT_ENABLED` acts as a flag for the Self app to check if data sharing is followed by payments. If it is set to 1, then the app will first initiate a payment transaction before data sharing.

## DEBUG

`DEBUG`: You can enable the backend logs by adding `DEBUG=self-sdk:*` to your environment variables. Currently `@self_id/self-platform-sdk` uses the `debug` package to log useful information, which can help during the debugging process. For more details, visit the [`debug`](https://www.npmjs.com/package/debug) package documentation.
