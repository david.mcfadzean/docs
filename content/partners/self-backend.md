---
title: Backend code ( Node.js )
---

This page explains how to configure a backend server to handle identity information from the SELF network.

## Demo-Ready Integration Sample

If you want to see a complete example attestation, check out the `self-sample-attestor-express` repository. There are several branches demonstrating various implementations.

- [Self login demo](https://gitlab.com/selfid-public/self-sample-attestor-express/-/tree/dish-self-login)
- [Self connect demo](https://gitlab.com/selfid-public/self-sample-attestor-express/-/tree/updated-self-login). Check [this](https://gitlab.com/selfid-public/self-sample-attestor-express/-/blob/updated-self-login/template/index.html#L42) sample code where `clientId` is accessed via localstorage (you can choose different method based on your requirement) to use it later to call `createBtn`.
- [express-ws usage demo](https://gitlab.com/selfid-public/self-sample-attestor-express/-/tree/fix/express-ws-sample/).

In this documentation, all the code is separated in chunks, so you can utilize them into an existing site or framework.

## Backend requirements

- Node version `16.14.2`. It should work with v18 but it isn't properly tested yet. Some known differences are documented below.
- You will need to add an POST API which is called via SELF. It is sent as `endpoint` in WS connection response. you can set it to anything you want. Code shown in steps 6, 7 & 8 should be inside same API endpoint.
- You will need to handle websockets to share information with your website.
- You will need separate API for payment (mentioned in step 9)

## Manual Integration

### Initialize SELF components

1. Install SDK in your node.js backend

   ```bash
   npm i @self_id/self-platform-sdk@latest
   ```

   **Note:** When testing or developing over long periods, be sure to re-run this command to ensure you have the latest version installed.

1. Add following environment variables to your `.env` file or a similar location. Change their values accordingly, using the data provided from the SELF marketplace:

   ```bash
   DOMAIN=https://sample-attestor-domain.com
   SELF_MARKETPLACE_TOKEN=sknsdinjkxs-12kjnjass
   ATTESTOR_NAME='AttestorName'
   ATTESTOR_DID=did:mdip:omni-xqmn-fyzz-qufd-r5l
   PUBLIC_KEY=moeF4PQx829nAN9f3kDG88XFwSaWYDL3JX
   PRIVATE_KEY=cUsZuFZ5vY18DBNtstMAa4HvucrAhpE3kTPSdSWeHBGxAgvxHkUi
   PUBLIC_KEY_HEX=031eb429c1d63ef1505e2e99ee5ede671271d7f369242ac603a3af72852eb0a135
   PRIVATE_KEY_HEX=d989dba8574f3a50613f0c826b6dfa9d85f4eefdd16c8a111a43e7cb38546305
   PAYMENT_ENABLED=1 //use 1 if payments are enabled otherwise no need to include
   ```

   For further details on environment variables, see the [Backend Environment Variables](./environment.md) doc.

   **Note:** Don't forget to add a method of importing these variables into your project. For example: `require('dotenv').config()`.

1. The component of your backend handling SELF identities needs the following imports:

   ```js
   const {
     checkRequiredForLogin,
     verifyAttestations,
     sendAttestation,
     IPNSHelper,
     getPreparedPayment,
     signTx
   } = require("@self_id/self-platform-sdk");
   ```

1. Initialize the IPNS Helper, an object which encapsulates all the IPNS related functionalities. This could be used to save data to IPFS, link CIDs to IPNS, etc:

   ```js
   const helper = new IPNSHelper();
   helper.initializeIPNS();
   ```

   `helper.initializeIPNS()` should work without additional values, but we also have an option to connect to different IPNS services by passing a domain and port:

   ```js
   const helper = new IPNSHelper();
   helper.initializeIPNS("https://beta-ipfs.yourself.dev", 443);
   ```

   You should initialize the IPNS helper class object at global/sever level. As this code logic uses sockets to connect with IPNS gateway server, we can initialize it only once. Therefore you must include above code in different file & export `helper` object, only then it can be accessed from any where.

   For example, if you only use SELF to sign users in to your site, you can get away with initializing it at a `/login` page. But if you intend to send attestations after signing in you would need to reuse the same IPFS connection.

1. Create the file `wsc.js` to handle websockets:

   ```js
   module.exports = {};
   ```

   `wsc.js` is a helper file for storing the Active Web Socket connections, as it is in an in-memory object. This cannot persist, meaning once the node.js server is restarted all data from the websockets are lost. We suggest using production services to retain websocket information.

1. Initialize the Express app:

    ```js
    const http = require("http");
    const express = require("express");
    const router = express.Router();
    const WebSocket = require('ws');
    const app = express();

    app.use(express.json({limit: '5mb'}));
    app.use(express.urlencoded({limit: '5mb', extended: true}));
    app.use('/', router);

    const PORT = 3000; // or use any PORT that you are going to use
    /**
    * Set express app's PORT (you can use environment variable if necessary)
    */
    app.set("port", PORT);
    /**
    * Create HTTP server.
    */
    const server = http.createServer(app);
    /**
    * Initialize the WebSocket server instance
    */
    const wss = new WebSocket.Server({ server });
    ```

### Configure Incoming Data Connection

The code in this section handles the websocket connection, and sends the required information for the SELF app to perform QR scans & start the login process.

1. Add Websocket related logic:

   ```js
   const wsc = require("../wsc.js");
   wss.on("connection", (ws, req) => {
     const userID = req.url.substr(1);
     console.log("userID :>> ", userID);
     wsc[userID] = ws;
     let randChallenge = Math.random();
     //send immediatly a feedback to the incoming connection
     const json = {
       endpoint: "/user/handle", // endpoint which you need to implement to handle SELF login flow
       challenge: randChallenge, // random integer for inducing randomenss into
       domain: process.env.DOMAIN || "http://localhost:3000", // if possible use ngrok for HTTPS connections as mobile app might not work with http
       attestorDID: process.env.ATTESTOR_DID,
       attestorName: process.env.ATTESTOR_NAME,
       attestorPublicKeyHex: process.env.PUBLIC_KEY_HEX,
       paymentEnabled: !!process.env.PAYMENT_ENABLED, // we can use true or false based on our requirement (if we dont plan to change this functionality later on)
       paymentEndpoint: "/prepare_payment" // not needed if payment is disabled.
     };
     ws.send(JSON.stringify(json));
   });
   ```

1. Listen to PORT (for receiving requests):

   ```js
   /**
    * Listen on provided port, on all network interfaces.
    */
   server.listen(PORT);
   server.on("error", (error) => {
     console.log(error); // or error reporting logic
   });
   server.on("listening", () => {
     console.log("listening to PORT", PORT);
   });
   ```
   <!-- Begin Wrap in details block, once Docs site is built -->
   You might encounter issues when running both server & websockets on the same port. If you are using `express`, we suggest using the [express-ws](https://www.npmjs.com/package/express-ws) package for WS support. If you do, the above code should look more like this:

   ```js
   const express = require("express");
   const app = express();
   const expressWs = require("express-ws")(app);
   const wsc = require("../wsc.js");
   const PORT = 3000; 
   const randomStr = () => Math.floor(Math.random() * Date.now()).toString(36);

   app.ws("/:userID", async (ws, req) => {
     const userID = req.params.userID || randomStr();
     wsc[userID] = ws;
     let randChallenge = Math.random();
     //send immediatly a feedback to the incoming connection
     const json = {
       endpoint: "/user/handle", // endpoint which you need to implement to handle SELF login flow
       challenge: randChallenge, // random integer for inducing randomenss into
       domain: process.env.DOMAIN || "http://localhost:3000", // if possible use ngrok for HTTPS connections as mobile app might not work with http
       attestorDID: process.env.ATTESTOR_DID,
       attestorName: process.env.ATTESTOR_NAME,
       attestorPublicKeyHex: process.env.PUBLIC_KEY_HEX,
       paymentEnabled: !!process.env.PAYMENT_ENABLED, // we can use true or false based on our requirement (if we dont plan to change this functionality later on)
       paymentEndpoint: "/prepare_payment" // not needed if payment is disabled.
     };
     ws.send(JSON.stringify(json));
   });

   app.listen(PORT);
   ```
   <!-- End details block -->

   Note that in the example above, `userID` is used to identify the websocket connection. This is generally sent via the frontend widget, but there are times it could be used to link an already present user with a new account (in cases of SELF connect buttons). Therefore it is recommended to store both `userID` & `randChallenge` in a session store (usually in a database).

### Decrypt and Verifying Claim Integrity

The code example in this section help determine if the claim sent in `userVP` is authentic, and contains all necessary details (requiredForLogin data/atoms) to continue with signup process.

1. Define the API endpoint. In the section above, the endpoint value is set as `/user/handle`:

    ```js
    router.post("/login", async (req, res) => {

    }
    ```

    The next two steps provide the contents of this endpoint.

1. Capture and process data from the user:
  
   ```js
   const { userVP, clientId, requestorDID } = req.body;
   const { success } = await verifyAttestations(userVP, helper);
   if (success) authorizeLogin(clientId);
   ```

1. Check if required data is present:

   ```js
   const reqforLoginPass = success
     ? await checkRequiredForLogin(response.data)
     : false;
   if (!reqforLoginPass) {
     res.send({
       success: false,
       data: null,
       error: "Some or all molecules required for login are absent"
     });
     return;
   } else {
     authorizeLogin(clientId, success.data);
     console.log("Authorize the web UI and pass any token that's needed");
     res.send({
       success: true
     });
   }
   ```

   Things to note here:

   - The `verifyAttestations` function checks authenticity, validity (expiry) of claims.
   - `reqforLoginPass` could be true even if you haven't set `requiredForLogin`. Therefore it is not necessary for all attesters, as it depends on your business logic. See [Required for login](requiredForLogin.md) for details on how to update `requiredForLogin` from the SELF Marketplace website.
   - You can distinguish between new user sign ups & logins if you store `requestorDID` in a database. Just create user records with the unique ID set as `requestorDID`, and check if user is already present.
   - You will need to handle the response when `reqforLoginPass` is `true`. It is necessary to send the `{success: true}` response after login is completed successfully, to let the SELF app know that everything worked as expected.
   - In case of an error, send `{error: "error message here"}`, where the value of the `error` key describes the the error/issue.

1. Moving outside of the API endpoint block, define the `authorizeLogin` method:

   ```js
   const authorizeLogin = (clientId, sharedData, claimData = {}) => {
     // if client is available, generate a new AccessToken for the
     // client and send needed information to frontend via WebSocket
     if (wsc[clientId]) {
       wsc[clientId].send(
         JSON.stringify({
           success: true,
           token: "af6s8df67sd6fa8fsasdgh78687a", // use JWT tokens (or other auth token) here these tokens can be used for maintaining sessions
           sharedData,
           claimData
         })
       );
     } else {
       debug("ClientId not found in hash table", clientId);
     }
   };
   ```

   The logic above can be modified as needed. Some things to note here:

    - The JSON string in `wsc[clientId].send()` is sent to the frontend. The `success` key instructs `createBtn` to execute a function provided as a second parameter. You'll learn about this functionality once you complete this guide and read [Frontend Code](./self-frontend.md).
    - The `token` key is an example to provide to a demo front end. In a production environment, an auth token would be generated by a JWT or similar library, and sent to a databases accessible by the frontend.
    - `sharedData` is an array of objects which contains details of attesters & data issued by them. `claimData` is data generated by your system which is saved into the user's genome. Both of these values are sent to the frontend as part of this example for simple testing, but in a production environment would be saved in a secured database.

   <!-- Editor's note: This line doesn't seem relevant to this section, unless `requestorDID` *should* be part of the response from `authorizeLogin`.
   - `requestorDID` should be used as user's unique identification (if needed for database queries).
   -->

### Issue Claims

This section explains how to create attestations. Here, the function `sendAttestation` from the SELF SDK acts as a wrapper that can:

- create attestations,
- encrypt attestation data,
- offload data so the SELF mobile app can download and decrypt it using its private key.

In order to issue attestations, you will need the receiver's public key, DiD, and the data (here named as `claimData`) you want to offload.

1. `claimData` should be formatted per atom definition to maintain consistency. In the following example `claimData` has `name` & `drivers_license` attributes (atoms). `name` & `drivers_license` should be present in the attester's availableMolecules. Both of these atoms will be offloaded as separate attestations:

    ```js
    const claimData = {
      name: "Thomas Edison",
      drivers_license: "RC45SDADY"
    };
    const { clientId, publicKey, requestorDID } = req.body;
    const keys = Object.keys(claimData);
    if (keys.length) {
      /*  this method takes all the data the attestor wants to send to a SELF user,
      makes it cryptographically secure and encrypted, and puts it on IPFS to make
      sure the SELF user gets the data. */

      const response = await sendAttestation(
        { publicKey, requestorDID },
        claimData,
        helper, // ipfs helper
        true
      ); // should be true
    }
    // to reflect the login on frontend with needed data
    authorizeLogin(clientId, atoms, claimData);
      ```

1. `claimData` can also create atoms with JSON objects as a value. For example, `name` can have `firstName` & `lastName` attributes. In such cases, the atom should be defined as `JSON` in the SELF Marketplace:

   ```js
   const claimData = {
     name: { firstName: "Thomas", lastName: "Edison" },
     drivers_license: "RC45SDADY"
   };
   ```

   Things to note here:

   - The code for receiving claims, authorizing logins, and issuing claims should be inside same `POST` API endpoint. This is the same endpoint defined in the `endpoint` attribute in step 1 of [Configure Incoming Data Connection](#configure-incoming-data-connection).
   - In this [logins.js](https://gitlab.com/selfid-public/self-sample-attestor-express/-/blob/updated-self-login/app/controllers/login.js#L50) file from our example attestor repository, you will see `claimData = { general: true };` is sent just after logging in. This is an empty attestation, not shown in a user's genome. It is added just to recognize a returning user.
   - Sending the `const claimData = { general: true };` attestation just after login is necessary only if you don't have any `requiredForLogin`.

### Enabling payments

Attesters can pay users for signing up to their website. In order to start sending money to users, go through the following steps:

1. Update your payment from the SELF Marketplace. Add a valid BTC amount here, then click **UPDATE**:

![image](https://docs.yourself.id/files/payments.png)

1. After updating payment, add funds to your wallet address. Your wallet address is the same as the `PUBLIC_KEY` environment variable, and also visible from the **Account Settings** menu in the SELF Marketplace. For more information on how to access environment variables, see [Backend Environment Variables](./environment.md).

1. To enable payments from the backend, add or update the `paymentEnabled` & `paymentEndpoint` fields. These fields are accessed by the SELF app on QR scan, and used to create signed transaction, which are broadcasted by the SELF app after successful a login operation.

1. Define the endpoint specified by `paymentEndpoint` to handle a POST API call, by adding following code. This should be separate from the API at `endpoint`:

   ```js
   const { fromAddr, toAddresses, opts } = req.body;
   const { blockchain, network, rawdata } = opts;

   const rawTx = await getPreparedPayment(fromAddr, toAddresses, opts);

   const nulldata = rawdata.startsWith("/ipns/") ? rawdata : null;
   const didInputs = {
     blockchain,
     network,
     nulldata,
     bypassDocChecks: true
   };
   return signTx(
     process.env.PRIVATE_KEY,
     rawTx,
     process.env.PUBLIC_KEY,
     didInputs
   );
   ```

   Things to note here:

   - The `paymentEnabled` should be a truthy value (for example: `1`). If you don't want to enable payment, just remove this field or set it as false (`0`).
   - The `paymentEndpoint` field is the API endpoint, at which attesters are supposed to handle payment signing requests from the mobile app.
   - The response of `signTx` is sent back to the mobile app, which is then broadcasted after the requested information is updated in IPNS.
   - The contents of `nulldata` is encrypted, & can only be decrypted by attesters.
   - You need to add check if the IPNS link is shared in `rawdata`, because this is where the shared data is stored permanently.

## Next Steps

- Now that your backend is configured, you can set up your [frontend](./self-frontend.md) to interact with it.
- Once your frontend is configured, test the setup using the SELF app from TestFlight.
- Update necessary things before production release (like replace token generation logic, websocket store, database support etc).
- Update domain name of backend & point to the correct domain from your frontend.