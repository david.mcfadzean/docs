# API access token

![image](https://docs.yourself.id/files/api-token.png)

You can create an API token so you can use it for calling APIs.

- Visit the API token access link from the top right menu.
- Add token label (as shown in screenshot below)
- Click ADD TOKEN button
- Save the generated API token (because it is visible only once)


API access token is used in backend environment setup.